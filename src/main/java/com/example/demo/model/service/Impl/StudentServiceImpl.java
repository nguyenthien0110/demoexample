package com.example.demo.model.service.Impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.demo.model.Student;
import com.example.demo.model.repository.StudentRepository;
import com.example.demo.model.service.StudentService;

@Service
public class StudentServiceImpl implements StudentService{

  @Autowired
  StudentRepository studentRepository;
  
  @Override
  public List<Student> findAll() {
    List<Student> list = studentRepository.findAll();
    return list;
  }

}
