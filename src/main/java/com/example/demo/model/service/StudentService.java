package com.example.demo.model.service;

import java.util.List;
import org.springframework.stereotype.Service;
import com.example.demo.model.Student;

@Service
public interface StudentService {

  public List<Student> findAll();
}
