package com.example.demo.model.repository;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import com.example.demo.model.Student;

@Mapper
public interface StudentRepository{

  List<Student> findAll();
}
