package com.example.demo.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.example.demo.model.Student;
import com.example.demo.model.service.StudentService;

@RestController
@RequestMapping("/api")
public class StudentController {

  @Autowired
  StudentService studentService;

  @GetMapping("/welcome")
  public String welcome() {
    return "hello";
  }

  @GetMapping("/getall")
  public List<Student> getAll() {
    return studentService.findAll();
  }
}
